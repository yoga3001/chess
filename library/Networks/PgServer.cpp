#include "stdafx.h"
#include "PgServer.h"


PgServer::PgServer(boost::asio::io_service& io_service, short port)
	: acceptor_(io_service, tcp::endpoint(tcp::v4(), port))
	, socket_(io_service)
{
	this->accept();
}


PgServer::~PgServer()
{
}

void PgServer::accept()
{
	this->acceptor_.async_accept(this->socket_,
		[this](boost::system::error_code ec) //lambda
		{
			if (!ec)
			{
				std::make_shared<PgSession>(std::move(socket_))->start();
			}
			accept();
		});
}
