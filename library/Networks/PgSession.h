#pragma once
#include "boost\asio.hpp"
#include "Ira\pgPacket.h"

using boost::asio::ip::tcp;

#define MAX_INET_PACKET_LEN 2048

class PgSession 
	: public std::enable_shared_from_this<PgSession>
{
private:
	tcp::socket sock_;
	
	char * data_buffer[2048];
public:
	PgSession(tcp::socket sock);

	~PgSession();

	void start();

private:
	void read_sock();

	void write_sock(std::size_t length);
};

