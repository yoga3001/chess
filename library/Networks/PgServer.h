#pragma once
#include "PgSession.h"

class PgServer
{
private:
	tcp::acceptor acceptor_;
	tcp::socket socket_;
public:
	PgServer(boost::asio::io_service& io_service, short port);

	~PgServer();

	void accept();
};

