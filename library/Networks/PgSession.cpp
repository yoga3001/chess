#include "stdafx.h"
#include "PgSession.h"


PgSession::PgSession(tcp::socket sock)
	:sock_(std::move(sock))
{
	memset(this->data_buffer, 0, MAX_INET_PACKET_LEN);
}


PgSession::~PgSession()
{
}

void PgSession::start()
{
	this->read_sock();
}

void PgSession::read_sock()
{
	auto self(shared_from_this());
	sock_.async_read_some(boost::asio::buffer(data_buffer, MAX_INET_PACKET_LEN), 
		[this, self](boost::system::error_code ec, std::size_t length) // lambda
		{
			if (!ec)
			{
				write_sock(length);
			}
		});
}

void PgSession::write_sock(std::size_t length)
{
	auto self(shared_from_this());
	boost::asio::async_write(this->sock_, boost::asio::buffer(this->data_buffer, MAX_INET_PACKET_LEN),
		[this, self] (boost::system::error_code ec, std::size_t length)// lambda
		{
			if (!ec)
			{
				read_sock();
			}
		});
}
