@echo off
SET CORE_PATH=D:\work\Durak\galaxy_src\

xcopy /Y %CORE_PATH%src\SFML-2.5.0\include\SFML\*.h .\galaxy\SFML-2.5.0\include\SFML\*.h
xcopy /Y %CORE_PATH%src\SFML-2.5.0\include\SFML\*.inl .\galaxy\SFML-2.5.0\include\SFML\*.inl
xcopy /Y %CORE_PATH%src\SFML-2.5.0\include\SFML\*.hpp .\galaxy\SFML-2.5.0\include\SFML\*.hpp

xcopy /Y %CORE_PATH%src\SFML-2.5.0\include\SFML\Audio\*.h .\galaxy\SFML-2.5.0\include\SFML\Audio\*.h
xcopy /Y %CORE_PATH%src\SFML-2.5.0\include\SFML\Audio\*.inl .\galaxy\SFML-2.5.0\include\SFML\Audio\*.inl
xcopy /Y %CORE_PATH%src\SFML-2.5.0\include\SFML\Audio\*.hpp .\galaxy\SFML-2.5.0\include\SFML\Audio\*.hpp

xcopy /Y %CORE_PATH%src\SFML-2.5.0\include\SFML\Graphics\*.h .\galaxy\SFML-2.5.0\include\SFML\Graphics\*.h
xcopy /Y %CORE_PATH%src\SFML-2.5.0\include\SFML\Graphics\*.inl .\galaxy\SFML-2.5.0\include\SFML\Graphics\*.inl
xcopy /Y %CORE_PATH%src\SFML-2.5.0\include\SFML\Graphics\*.hpp .\galaxy\SFML-2.5.0\include\SFML\Graphics\*.hpp

xcopy /Y %CORE_PATH%src\SFML-2.5.0\include\SFML\Network\*.h .\galaxy\SFML-2.5.0\include\SFML\Network\*.h
xcopy /Y %CORE_PATH%src\SFML-2.5.0\include\SFML\Network\*.inl .\galaxy\SFML-2.5.0\include\SFML\Network\*.inl
xcopy /Y %CORE_PATH%src\SFML-2.5.0\include\SFML\Network\*.hpp .\galaxy\SFML-2.5.0\include\SFML\Network\*.hpp

xcopy /Y %CORE_PATH%src\SFML-2.5.0\include\SFML\System\*.h .\galaxy\SFML-2.5.0\include\SFML\System\*.h
xcopy /Y %CORE_PATH%src\SFML-2.5.0\include\SFML\System\*.inl .\galaxy\SFML-2.5.0\include\SFML\System\*.inl
xcopy /Y %CORE_PATH%src\SFML-2.5.0\include\SFML\System\*.hpp .\galaxy\SFML-2.5.0\include\SFML\System\*.hpp

xcopy /Y %CORE_PATH%src\SFML-2.5.0\include\SFML\Window\*.h .\galaxy\SFML-2.5.0\include\SFML\Window\*.h
xcopy /Y %CORE_PATH%src\SFML-2.5.0\include\SFML\Window\*.inl .\galaxy\SFML-2.5.0\include\SFML\Window\*.inl
xcopy /Y %CORE_PATH%src\SFML-2.5.0\include\SFML\Window\*.hpp .\galaxy\SFML-2.5.0\include\SFML\Window\*.hpp

xcopy /Y %CORE_PATH%src\SFML-2.5.0\lib\*.lib .\galaxy\SFML-2.5.0\lib\*.lib
xcopy /Y %CORE_PATH%src\SFML-2.5.0\lib\*.pdb .\galaxy\SFML-2.5.0\lib\*.pdb
xcopy /Y %CORE_PATH%src\SFML-2.5.0\lib\*.idb .\galaxy\SFML-2.5.0\lib\*.idb
xcopy /Y %CORE_PATH%src\SFML-2.5.0\bin\*.dll .\galaxy\SFML-2.5.0\bin\*.dll
pause