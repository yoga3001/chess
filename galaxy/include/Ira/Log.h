#pragma once
#include <Windows.h>

namespace Ira
{
	enum LogType
	{
		LOG_INFO	= 1,
		LOG_WARNING = 2,
		LOG_ERROR	= 4,
		LOG_DEBUG	= 8,
		LOG_ALL		= LOG_INFO | LOG_WARNING | LOG_ERROR | LOG_DEBUG,
	};

	enum LogWriteType {
		LWT_NO_LOG		= 0,
		LWT_CONSOLE_LOG = 1,
		LWT_FILE_LOG	= 2,

		LWT_ALL			= LWT_FILE_LOG | LWT_CONSOLE_LOG,
	};
	class Log
	{
	private:
		DWORD WriteType;
		DWORD OutLogType;
		char strFileName[255];
	public:
		Log();
		~Log();
		//! Init Log
		void Init(const char* strFileName, DWORD const WriteType = LWT_ALL, DWORD const OutLogType = LOG_ALL);

		//! Write log
		void WriteLog(LogType const kLogType, const char * strMsg, ...);

	private:
		//! Write to file
		void WriteToFile(const char* LogText);

		void WriteToConsole(LogType const kLogType, const char* LogText);
	};
}