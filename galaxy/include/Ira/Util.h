#pragma once
#include <Windows.h>
#include <string>

namespace Ira
{
	DWORD GetTime();

	std::string GetTime(const char* strTimeFormat = "%d.%m.%Y %H:%M:%S");
}