//
// Created by reoil on 29.07.18.
//

#ifndef FILEBINARY_BINPATH_H
#define FILEBINARY_BINPATH_H

#include <map>
#include "FileBase.h"

namespace Ira {

    //! Path data in offset
    bool PathMemory(std::vector<char> &MemIn, unsigned int const OffsetAdders, const char *chData);

    //! Path file by offset
    bool PathFile(const char *FileNameSrc,const char *FileNameDst, unsigned int const OffsetAdders, const char *Data);

}

#endif //FILEBINARY_BINPATH_H
