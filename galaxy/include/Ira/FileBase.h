//
// Created by reoil on 28.07.18.
//

#ifndef FILEBINARY_FILEBASE_H
#define FILEBINARY_FILEBASE_H

#include <vector>

namespace Ira {

    //! Get memory to File
    bool MemFromFile(const char *strFileName, std::vector<char> &DataOut);

    //! Save memory to file
    bool MemToFile(const char *strFileName, std::vector<char> const DataIn);

}
#endif //FILEBINARY_FILEBASE_H
