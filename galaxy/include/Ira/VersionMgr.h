#pragma once

namespace Ira
{
	struct sGameVersion
	{
		sGameVersion(const short Minor = 0, const short Major = 0, const short Tiny = 0)
			: shMinor(Minor)
			, shMajor(Major)
			, shTiny(Tiny)
		{
		}

		short shMinor;
		short shMajor;
		short shTiny;
	};

	sGameVersion ReadVersion();
	
	void MakeVersion(sGameVersion const &VersionInfo);
}
