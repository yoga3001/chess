@echo off
SET CORE_PATH=D:\work\Durak\galaxy_src\

REM Tiny Xml
xcopy /Y %CORE_PATH%src\tinyxml2\*.h .\galaxy\include\tinyxml2\*.h
xcopy /Y %CORE_PATH%src\tinyxml2\*.inl .\galaxy\include\tinyxml2\*.inl
xcopy /Y %CORE_PATH%src\tinyxml2\*.hpp .\galaxy\include\tinyxml2\*.hpp

REM Ira
xcopy /Y %CORE_PATH%src\Ira\*.h .\galaxy\include\Ira\*.h
xcopy /Y %CORE_PATH%src\Ira\*.inl .\galaxy\include\Ira\*.inl
xcopy /Y %CORE_PATH%src\Ira\*.hpp .\galaxy\include\Ira\*.hpp

REM GUI
REM xcopy /Y %CORE_PATH%src\GUI\*.h .\galaxy\include\GUI\*.h
REM xcopy /Y %CORE_PATH%src\GUI\*.inl .\galaxy\include\GUI\*.inl
REM xcopy /Y %CORE_PATH%src\GUI\*.hpp .\galaxy\include\GUI\*.hpp

REM Boost lib
xcopy /Y /I %CORE_PATH%src\boost_1_68_0\boost\*.h .\galaxy\include\boost\*.h
xcopy /Y /I %CORE_PATH%src\boost_1_68_0\boost\*.inl .\galaxy\include\boost\*.inl
xcopy /Y /I %CORE_PATH%src\boost_1_68_0\boost\*.hpp .\galaxy\include\boost\*.hpp

REM Sfml
REM call sfml_sync.bat

xcopy /Y %CORE_PATH%lib\*.lib .\galaxy\lib\*.lib
xcopy /Y %CORE_PATH%lib\*.pdb .\galaxy\lib\*.pdb
xcopy /Y %CORE_PATH%lib\*.idb .\galaxy\lib\*.idb
REM Boost library
xcopy /Y %CORE_PATH%src\boost_1_68_0\stage\win32\lib\*.lib .\galaxy\lib\*.lib
pause