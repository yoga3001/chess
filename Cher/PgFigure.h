#pragma once
#include "PgObject.h"
#include "Ira\ClassSupport.h"

class PgFigure :
	public PgObject
{
protected:
	EFigureType kFigureType;
	EFigureColor kFigureColor;
public:
	PgFigure();

	PgFigure(std::string strObjID);

	virtual ~PgFigure();

	ObjectType GetObjectType() const { return OT_FIGURE; }

	void SetCustumType(structObjXmlElement const  rhs);

	EFigureColor const FigureColor() { return kFigureColor; }

	EFigureType const FigureType() { return kFigureType; }

	bool Move(sf::Vector2u const vecNewPos);
};

