#include "stdafx.h"
#include "PgObjectManager.h"
#include "PgRender.h"
#include "PgUtil.h"
#include <iostream>
#include "PgGameProcess.h"

PgObjectManager g_kObjectMgr;

PgObjectManager::PgObjectManager()
{
	kFiled = NULL;
}


PgObjectManager::~PgObjectManager()
{
	for (auto it : this->kObjectList)
	{
		delete it;
	}
	this->kObjectList.clear();
}

void PgObjectManager::LoadXml()
{
	tinyxml2::XMLDocument kDoc;
	if (kDoc.LoadFile("xml/Object.Xml") != tinyxml2::XMLError::XML_SUCCESS)
	{
		return;
	}

	auto kElement = kDoc.FirstChildElement();
	while (kElement)
	{
		if (!::strcmp(kElement->Name(), "SHEET"))
		{
			auto kChiledElement = kElement->FirstChildElement();
			while (kChiledElement)
			{
				if (!::strcmp("OBJECT", kChiledElement->Name()))
				{
					structObjXmlElement kStructOXE;
					ParseObject(kChiledElement, kStructOXE);
					MakeXmlObjectPool(kStructOXE);
				}
				else if (!::strcmp("BUILD", kChiledElement->Name()))
				{
					ParseBuild(kChiledElement);
				}
				kChiledElement = kChiledElement->NextSiblingElement();
			}
		}
		kElement = kElement->NextSiblingElement();
	}

	for (auto it: this->kObjectList)
	{
		if (it && it->GetObjectType() == OT_FIGURE)
		{
			g_kGameProcess.AddFigureToFiled(dynamic_cast<PgFigure *>(it));
		}
	}
}

void PgObjectManager::one_tick()
{
	for (auto it : this->kObjectList)
	{
		g_kRender.AddDrawObject(it);
	}
}

void PgObjectManager::CollideAllObjectWhitPoint(sf::Vector2f const vecPointPos)
{
	if (g_kGameProcess.IsOver())// if game over do noting
	{ 
		return;
	}

	if (kSelectedFigure && kSelectedFigure->IsSelect() == true && kSelectedFigure->IsVisible())
	{

		for (auto jt : kFiled->GetDrawRect())
		{
			if (jt.getGlobalBounds().contains(vecPointPos))
			{
				//kSelectedFigure->Move(PgGameProcess::ConvPosToCells(vecPointPos));
				// note + AJA_FIGURE_X and + AJA_FIGURE_Y that because convert to cels pos it for model, i will remake it latter -,- 
				if (g_kGameProcess.MoveFigure(kSelectedFigure, sf::Vector2f(jt.getPosition().x + AJA_FIGURE_X, jt.getPosition().y + AJA_FIGURE_Y))) { // if it have enemy figure it will be delete
					g_kGameProcess.SwapStage();
					kFiled->ClearRect(); // clear all move rect in filed
					kSelectedFigure->DeSelect();
					return; // exit
				}
				//todo added move function, but first need check if he can eat this
			}
		}
	}

	kFiled->ClearRect(); // clear all move rect in filed
	for (auto it : this->kObjectList)
	{
		it->DeSelect();
	}

	for (auto it: this->kObjectList)
	{
		if (it->GetObjectType() == OT_FIGURE)
		{
			PgFigure* kFigure = dynamic_cast<PgFigure*>(it);
			if (kFigure && kFigure->FigureColor() == g_kGameProcess.WhoCourseNow()
				&& kFigure->GetSprite().getGlobalBounds().contains(vecPointPos) && kFigure->IsVisible())
			{
				kFigure->Select();
				kSelectedFigure = kFigure;
				break;
			}
		}
	}

	if (kSelectedFigure && kSelectedFigure->IsSelect() == true && kSelectedFigure->IsVisible())
	{
		auto kMoveAllPos = g_kGameProcess.GetAllMovePos(kSelectedFigure);
		for (auto it : kMoveAllPos)
		{
			if (g_kGameProcess.CheckEnemyOnPos(it, kSelectedFigure->FigureColor()))
			{
				kFiled->SelectRect(it, sf::Color::Red);
			}
			else
			{
				if (g_kGameProcess.IsEmptyPos(it))
				{
					kFiled->SelectRect(it, sf::Color::Green);
				}
			}
		}
	}
}

void PgObjectManager::EraseObject(std::string const strObjID)
{
	for (size_t i = 0; i < this->kObjectList.size(); i++)
	{
		if (kObjectList.at(i) && kObjectList.at(i)->ID() == strObjID)
		{
			delete kObjectList.at(i);
			kObjectList.erase(kObjectList.begin() + i);
			break;
		}
	}
}

PgObject * PgObjectManager::FindObjByID(std::string strID)
{
	for (auto it: this->kObjectList)
	{
		if (it->ID() == strID)
		{
			return it;
		}
	}
	return NULL;
}

void PgObjectManager::MakeXmlObjectPool(structObjXmlElement const kObjXmlElemnt)
{
	switch ((ObjectType)kObjXmlElemnt.dwObjType)
	{
		case OT_OBJECT: { this->AddObject<PgObject>(kObjXmlElemnt); }break;
		case OT_FIELD: { this->AddObject<PgField>(kObjXmlElemnt); }break;
		case OT_FIGURE: { this->AddObject<PgFigure>(kObjXmlElemnt); }break;
	}
}

void PgObjectManager::ParseObjAttr(tinyxml2::XMLAttribute const * kAttr, structObjXmlElement & rhs)
{
	while (kAttr)
	{
		const char* kName = kAttr->Name();
		const char* kValued = kAttr->Value();
		if (!::strcmp("TYPE", kName))
		{
			rhs.dwObjType = strcmp("FIELD", kValued) == 0 ? OT_FIELD :
				strcmp("FIGURE", kValued) == 0 ? OT_FIGURE : OT_OBJECT;
		}
		else if (!::strcmp("TEXTURE_PATH", kName))
		{
			rhs.strTexturePath = kValued;
		}
		else if (!::strcmp("POS", kName))
		{
			auto kSpliteList = SplitString<float>(kValued, std::regex("[\\d*\\.\\d*]+"));
			rhs.vecPos.x = kSpliteList.size() > 0 ? kSpliteList.at(0) : 0;
			rhs.vecPos.y = kSpliteList.size() > 1 ? kSpliteList.at(1) : 0;
		}
		else if(!::strcmp("FIGURE_TYPE", kName))
		{
			rhs.kFigureType = (EFigureType)atoi(kValued);
		}
		else if (!::strcmp("FIGURE_COLOR", kName))
		{
			rhs.kFigureColor = (EFigureColor)atoi(kValued);
		}
		else if (!::strcmp("ID", kName))
		{
			rhs.strID = kValued;
		}
		else if (!::strcmp("IS_VISIBLE", kName))
		{
			rhs.bIsVisible = ::strcmp("TRUE", kValued) == 0 ? true : false;
		}
		kAttr = kAttr->Next();
	}
}

void PgObjectManager::ParseObject(tinyxml2::XMLElement const * kElement, structObjXmlElement & rhs)
{
	if (kElement == NULL)
	{
		return;
	}

	if (!::strcmp("OBJECT", kElement->Name()))
	{
		ParseObjAttr(kElement->FirstAttribute(), rhs);
	}
}

void PgObjectManager::ParseBuild(tinyxml2::XMLElement const * kElement)
{
	if (kElement == NULL)
	{
		return;
	}

	if (!::strcmp("BUILD", kElement->Name()))
	{
		structObjXmlElement kStructOXE;
		tinyxml2::XMLAttribute const * kAttr = kElement->FirstAttribute();
		size_t iBuildCount = 0;
		std::pair<std::string, int> kCustumType;
		while (kAttr)
		{
			auto kName = kAttr->Name();
			auto kValued = kAttr->Value();
			if (!::strcmp("BUILD_COUNT", kName))
			{
				iBuildCount = (size_t)atoi(kValued);
			}
			else if (!::strcmp("CUSTUM_TYPE", kName))
			{
				kCustumType.first = kValued;
			}
			else if (!::strcmp("CUSTUM_ITER", kName))
			{
				kCustumType.second = atoi(kValued);
			}

			kAttr = kAttr->Next();
		}

		tinyxml2::XMLElement const * kChiledElement = kElement->FirstChildElement();
		if (kChiledElement)
		{
			structObjXmlElement kStructOXE;
			ParseObject(kChiledElement, kStructOXE);
			//////////////////////////////////////////
			for (size_t i = 0; i < iBuildCount; i++)
			{
				if (kCustumType.first == "POS" && i != 0)
				{
					kStructOXE.vecPos.x = kStructOXE.vecPos.x + kCustumType.second;
				}
				MakeXmlObjectPool(kStructOXE);
			}
			//////////////////////////////////////////
			kChiledElement = kChiledElement->NextSiblingElement();
		}
	}
}
