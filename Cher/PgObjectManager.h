#pragma once
#include "PgField.h"
#include "PgFigure.h"
#include "tinyxml2\tinyxml2.h"
#include "PgConfig.h"

class PgObjectManager
{
private:
	std::vector<PgObject*> kObjectList;

	PgField* kFiled;

	PgFigure* kSelectedFigure;
public:
	PgObjectManager();

	~PgObjectManager();

	void LoadXml();

	void one_tick();
	
	template <typename T>
	void AddObject(structObjXmlElement const kObjXmlElemnt);

	void CollideAllObjectWhitPoint(sf::Vector2f const vecPointPos);

	void EraseObject(std::string const strObjID);

	PgObject * FindObjByID(std::string strID);
private: // XML Function only!
	void MakeXmlObjectPool(structObjXmlElement const kObjXmlElemnt);

	void ParseObjAttr(tinyxml2::XMLAttribute const* kAttr, structObjXmlElement &rhs);
	
	void ParseObject(tinyxml2::XMLElement const * kElement, structObjXmlElement &rhs);

	void ParseBuild(tinyxml2::XMLElement const * kElement);
};

extern PgObjectManager g_kObjectMgr;

template<typename T>
inline void PgObjectManager::AddObject(structObjXmlElement const kObjXmlElemnt)
{
	T* kTemp = new T;
	if (kObjXmlElemnt.strID.size() != 0)
	{
		delete kTemp;
		kTemp = new T(kObjXmlElemnt.strID);
	}

	if (!kTemp)
	{
		return;
	}
	sf::Vector2f kTempCordinate(kObjXmlElemnt.vecPos.x * fScale, kObjXmlElemnt.vecPos.y * fScale);
	kTemp->SetTextute(kObjXmlElemnt.strTexturePath);
	kTemp->Position(kTempCordinate);
	kTemp->SetCustumType(kObjXmlElemnt);
	kTemp->IsVisible(kObjXmlElemnt.bIsVisible);
	if (kTemp->GetObjectType() == OT_FIELD)
	{
		kFiled = dynamic_cast<PgField*>(kTemp);
	}
	kObjectList.push_back(kTemp);
}
