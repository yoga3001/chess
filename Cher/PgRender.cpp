#include "stdafx.h"
#include "PgRender.h"
#include "PgEventHandler.h"
#include "SFML/Graphics.hpp"
#include "PgObjectManager.h"

PgRender g_kRender;

PgRender::PgRender()
{
}


PgRender::~PgRender()
{
	this->m_kWnd.close();
}

bool PgRender::Init(sf::VideoMode const & kWindowMode, const char * strWndTitle)
{
	this->m_kWnd.create(kWindowMode, strWndTitle, sf::Style::Close | sf::Style::Titlebar);
	return true;
}

void PgRender::DoRender()
{
	this->m_kWnd.clear(); // Clear currect wnd
	for (auto it = this->kRenderObject.begin(); it != this->kRenderObject.end(); it++)
	{
		if ((*it)->IsVisible() == false) continue; // if this obj is hide

		this->m_kWnd.draw((*it)->GetSprite());
		if ((*it)->GetObjectType() == OT_FIELD) // if object is filed we must render move box trace (green box)
		{
			PgField* kTempFiled = dynamic_cast<PgField*>((*it));
			if (kTempFiled)
			{
				auto kDrawTaskFiled = kTempFiled->GetDrawRect();
				for (auto it: kDrawTaskFiled)
				{
					this->m_kWnd.draw(it);
				}
			}
		}
	}
	this->kRenderObject.clear(); // reset draw object
	this->m_kWnd.display(); // show wnd
	// Callback event
	EventWork();
	g_kEventHandle.Release();
}

void PgRender::AddDrawObject(PgObject * ObjectPtr, bool bFront)
{
	if (bFront)
	{
		this->kRenderObject.push_front(ObjectPtr);
	}
	else
	{
		this->kRenderObject.push_back(ObjectPtr);
	}
}

void PgRender::EventWork()
{
	sf::Event kEvent;
	while (this->m_kWnd.pollEvent(kEvent))
	{
		g_kEventHandle.InsertEvent(kEvent);
	}
}

