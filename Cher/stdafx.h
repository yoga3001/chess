// stdafx.h: ���������� ���� ��� ����������� ��������� ���������� ������
// ��� ���������� ������ ��� ����������� �������, ������� ����� ������������, ��
// �� ����� ����������
//

#pragma once
#define FILE_VERSION "1.0.0.5"
#define PROD_VERSION "1.0.0.5"

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
#include <Windows.h>

#pragma comment(lib, "flac.lib")
#pragma comment(lib, "vorbisenc.lib")
#pragma comment(lib, "vorbisfile.lib")
#pragma comment(lib, "vorbis.lib")
#pragma comment(lib, "ogg.lib")
#pragma comment(lib, "openal32.lib")
#pragma comment(lib, "freetype.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "winmm.lib")

#ifdef _MD_
#pragma comment(lib, "Ira_MD.lib")
#pragma comment(lib, "sfml-graphics-s.lib")
#pragma comment(lib, "sfml-window-s.lib")
#pragma comment(lib, "sfml-audio-s.lib")
#pragma comment(lib, "sfml-system-s.lib")
#pragma comment(lib, "tinyxml2_MD.lib")
#endif // _MD_

#ifdef _MDd_
#pragma comment(lib, "Ira_MDd.lib")
#pragma comment(lib, "sfml-graphics-d.lib")
#pragma comment(lib, "sfml-window-d.lib")
#pragma comment(lib, "sfml-audio-d.lib")
#pragma comment(lib, "sfml-system-d.lib")
#pragma comment(lib, "tinyxml2_MDd.lib")
#endif // _MD_

#define AJA_FIGURE_X 45

#define AJA_FIGURE_Y 25

#define NBOX_OUT(vir1) \
(8 > (int)vir1 && (int)vir1 > -1)

#define IFNBOX_OUT(vir1) \
if(NBOX_OUT(vir1))

#define NBOX_OUT2(vir1, vir2) \
NBOX_OUT(vir1) && NBOX_OUT(vir2)

#define IFNBOX_OUT2(vir1, vir2) \
if(NBOX_OUT(vir1) && NBOX_OUT(vir2))
// TODO: ���������� ����� ������ �� �������������� ���������, ����������� ��� ���������
