#pragma once
#include "SFML/Graphics.hpp"
#include <string>
#include "Ira\ClassSupport.h"

enum ObjectType
{
	OT_OBJECT = 1,
	OT_FIELD = 2,
	OT_FIGURE = 3,
	OT_FIELD_NODE = 4,
};

enum EFigureType
{
	EFT_PAWN		= 1,
	EFT_HORSE		= 2,
	EFT_ELEPHANT	= 3,
	EFT_ROOK		= 4,
	EFT_KING		= 5,
	EFT_QUEEN		= 6,
};

enum EFigureColor
{
	EFC_BLACK = 1,
	EFC_WHITE = 2,
};

struct structObjXmlElement
{
	std::string strID = "";
	bool bIsVisible = true;
	DWORD dwObjType = 0;
	std::string strTexturePath;
	sf::Vector2f vecPos;
	// this only for PgFigure.h
	EFigureType kFigureType;
	EFigureColor kFigureColor;
	// this only for PgField.h
};

class PgObject
{
private:
	sf::Sprite kSprite;

	sf::Texture kTexture;

	bool bIsSelect;
public:
	PgObject();

	PgObject(std::string strObjID);
	
	virtual ~PgObject();

	void SetTextute(std::string const strTexturePath);

	sf::Sprite GetSprite() const { return kSprite; }

	void Position(sf::Vector2f const kPos);

	sf::Vector2f Position() const;

	virtual ObjectType GetObjectType() const { return OT_OBJECT; }

	virtual void SetCustumType(structObjXmlElement const rhs) {};

	// Make Object selected
	void Select();
	// Make Object deselected
	void DeSelect();

	bool IsSelect() const { return this->bIsSelect; };

	CLASS_DECLARATION_NO_SET(std::string, m_kID, ID)
	CLASS_DECLARATION_S(bool, IsVisible)
};

