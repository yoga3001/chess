#pragma once
#include "PgObject.h"

class PgField :
	public PgObject
{
private:
	std::vector<sf::RectangleShape> vecFiledRect;
public:
	PgField();

	PgField(std::string strObjID);

	virtual ~PgField();

	ObjectType GetObjectType() const { return OT_FIELD; }

	void SetCustumType(structObjXmlElement const rhs);

	std::vector<sf::RectangleShape> GetDrawRect() { return this->vecFiledRect; };

	void ClearRect();

	void SelectRect(sf::Vector2u const& vecPoint, sf::Color kColor = sf::Color::Green);
};

