#include "stdafx.h"
#include "PgGameProcess.h"
#include "PgSoundManager.h"
#include "PgConfig.h"

PgGameProcess g_kGameProcess;

PgGameProcess::PgGameProcess()
{
	this->eWhoCourseNow = EFC_WHITE;
	this->kFiledEmulate.resize(8);
	for (size_t i = 0; i < 8; i++)
	{
		this->kFiledEmulate.at(i).resize(8);
	}
	this->bIsOver = false;
	bCanEnPassant = false;
}


PgGameProcess::~PgGameProcess()
{
}

sf::Vector2u const PgGameProcess::ConvPosToCells(sf::Vector2f const inpVec)
{
	return sf::Vector2u(((unsigned int)((inpVec.x / fScale - AJA_FIGURE_X) / 90) + 0.5), ((unsigned int)((inpVec.y / fScale - AJA_FIGURE_Y) / 90) + 0.5));
}

sf::Vector2f const PgGameProcess::ConvCellsToPos(sf::Vector2u const inpVec)
{
	return sf::Vector2f((float)((AJA_FIGURE_X + (inpVec.x * 90)) * fScale) + 0.5, (float)((AJA_FIGURE_Y + (inpVec.y * 90)) * fScale) + 0.5);
}

MovePos const PgGameProcess::GetAllMovePos(PgFigure * kFigure)
{
	MovePos MovePosRet;
	if (!kFigure) { return MovePosRet; }
	sf::Vector2i PosBoard(this->ConvPosToCells(kFigure->GetSprite().getPosition()));
	switch (kFigure->FigureType())
	{
	case EFT_PAWN:		{ return PawnMoveDir(PosBoard, kFigure->FigureColor()); }break;
	case EFT_HORSE:		{ return HorseMoveDir(PosBoard, kFigure->FigureColor()); }break;
	case EFT_ELEPHANT:	{ return ElephantMoveDir(PosBoard, kFigure->FigureColor()); }break;
	case EFT_ROOK:		{ return RookMoveDir(PosBoard, kFigure->FigureColor()); }break;
	case EFT_KING:		{ return KingMoveDir(PosBoard, kFigure->FigureColor()); }break;
	case EFT_QUEEN:		{ return QueenMoveDir(PosBoard, kFigure->FigureColor()); }break;
	}
	return MovePosRet;
}

bool PgGameProcess::CheckCanMoveToPos(PgFigure * kFigure, sf::Vector2u const & kVec)
{
	auto kAllPos = GetAllMovePos(kFigure);
	for (auto it: kAllPos)
	{
		if (kVec == it)
		{
			return true;
		}
	}

	return false;
}


void PgGameProcess::SwapStage()
{
	if (this->eWhoCourseNow == EFC_BLACK)
	{
		this->eWhoCourseNow = EFC_WHITE;
	}
	else if (this->eWhoCourseNow == EFC_WHITE)
	{
		this->eWhoCourseNow = EFC_BLACK;
	}
}

void PgGameProcess::AddFigureToFiled(PgFigure * kFigure)
{
	if (kFigure == NULL)
	{
		return;
	}

	sf::Vector2i kFigurePos = static_cast<sf::Vector2i>(this->ConvPosToCells(kFigure->Position()));
	IFNBOX_OUT2(kFigurePos.x, kFigurePos.y)
	{
		this->kFiledEmulate.at(kFigurePos.y).at(kFigurePos.x) = kFigure;
	}
}

bool PgGameProcess::MoveFigure(PgFigure * kFigure, sf::Vector2f const& kNewPos)
{
	sf::Vector2i kFigurePos = static_cast<sf::Vector2i>(this->ConvPosToCells(kFigure->Position()));
	sf::Vector2i kFigureNewPos = static_cast<sf::Vector2i>(this->ConvPosToCells(kNewPos));
	IFNBOX_OUT2(kFigureNewPos.x, kFigureNewPos.y)
	{
		if (!this->CheckCanMoveToPos(kFigure, static_cast<sf::Vector2u>(kFigureNewPos)))
		{
			return false; // hack user!!!
		}

		if (this->kFiledEmulate.at(kFigureNewPos.y).at(kFigureNewPos.x)) // if filed have any anouther figure
		{
			if (this->kFiledEmulate.at(kFigureNewPos.y).at(kFigureNewPos.x)->FigureColor() == kFigure->FigureColor()) // if it have same color do nothing
			{
				return false;
			}

			if (this->kFiledEmulate.at(kFigureNewPos.y).at(kFigureNewPos.x)->FigureType() == EFT_KING) // King is die game over)
			{
				this->bIsOver = true;
				this->ShowWinDlg(kFigure->FigureColor());
			}

			g_kObjectMgr.EraseObject(this->kFiledEmulate.at(kFigureNewPos.y).at(kFigureNewPos.x)->ID());// delete old figure from filed
			this->kFiledEmulate.at(kFigureNewPos.y).at(kFigureNewPos.x) = NULL; // set point to NULL
			//g_kSoundMgr.ReleaseSound("EAT");
		}

		if (this->bCanEnPassant)
		{
			auto kTempFigure = dynamic_cast<PgFigure *>(this->kFiledEmulate.at(enPassantPos.y).at(enPassantPos.x));
			if (kTempFigure && 
				kTempFigure->FigureType() == EFT_PAWN && 
				kTempFigure->FigureColor() != kFigure->FigureColor())
			{
				// eat by en Passant
				g_kObjectMgr.EraseObject(this->kFiledEmulate.at(enPassantPos.y).at(enPassantPos.x)->ID());// delete old figure from filed
				this->kFiledEmulate.at(enPassantPos.y).at(enPassantPos.x) = NULL; // set point to NULL
			}
		}

		// move figure to new position
		this->kFiledEmulate.at(kFigureNewPos.y).at(kFigureNewPos.x) = kFigure;
		this->kFiledEmulate.at(kFigurePos.y).at(kFigurePos.x) = NULL;
		kFigure->Move(static_cast<sf::Vector2u>(kFigureNewPos));
		// >>>>>> en passant
		this->bCanEnPassant = false;
		if (kFigure->FigureType() == EFT_PAWN)
		{
			if (kFigure->FigureColor() == EFC_WHITE)
			{
				this->bCanEnPassant = (kFigureNewPos.y - kFigurePos.y) == 2;
			}

			if (kFigure->FigureColor() == EFC_BLACK)
			{
				this->bCanEnPassant = (kFigurePos.y - kFigureNewPos.y) == 2;
			}
			
			if(this->bCanEnPassant &&
				(this->CheckEnemyOnPos(static_cast<sf::Vector2u>(sf::Vector2i(kFigureNewPos.x - 1, kFigureNewPos.y)), kFigure->FigureColor())
				|| this->CheckEnemyOnPos(static_cast<sf::Vector2u>(sf::Vector2i(kFigureNewPos.x + 1, kFigureNewPos.y)), kFigure->FigureColor())))
			{
				this->enPassantPos = static_cast<sf::Vector2u>(kFigureNewPos);
			}
			// read more https://ru.wikipedia.org/wiki/%D0%92%D0%B7%D1%8F%D1%82%D0%B8%D0%B5_%D0%BD%D0%B0_%D0%BF%D1%80%D0%BE%D1%85%D0%BE%D0%B4%D0%B5
		}
		// <<<<<< en passant
		//g_kSoundMgr.ReleaseSound("MOVE");
		return true;
	}
	return false;
}

bool PgGameProcess::IsEmptyPos(sf::Vector2u const & kVec)
{
	IFNBOX_OUT2((int)kVec.x, (int)kVec.y)
	{
		return this->kFiledEmulate.at(kVec.y).at(kVec.x) == NULL;
	}
	return false;
}

bool PgGameProcess::CheckEnemyOnPos(sf::Vector2u const & kVec, EFigureColor const & eColor)
{
	IFNBOX_OUT2((int)kVec.x, (int)kVec.y)
	{
		if (!IsEmptyPos(kVec))
		{
			return this->kFiledEmulate.at(kVec.y).at(kVec.x)->FigureColor() != eColor;
		}
	}
	return false;
}

#define CHECK_CAN_MOVE(X, Y, C) \
	if (IsEmptyPos(sf::Vector2u(X, Y)) || CheckEnemyOnPos(sf::Vector2u(X, Y), C))

void PgGameProcess::HelpUI()
{
	auto kHelpUIBG = g_kObjectMgr.FindObjByID("OBJ_HELP_UI_BG");
	if (kHelpUIBG)
	{
		kHelpUIBG->IsVisible(true);
		auto kHelpUIBlack = g_kObjectMgr.FindObjByID("OBJ_HELP_UI_BLACK");
		if(kHelpUIBlack)
		{
			kHelpUIBlack->IsVisible(false);
			if (eWhoCourseNow == EFC_BLACK)
			{
				kHelpUIBlack->IsVisible(true);
			}
		}

		auto kHelpUIWhite = g_kObjectMgr.FindObjByID("OBJ_HELP_UI_WHITE");
		if (kHelpUIWhite)
		{
			kHelpUIWhite->IsVisible(false);
			if (eWhoCourseNow == EFC_WHITE)
			{
				kHelpUIWhite->IsVisible(true);
			}
		}
	}
}

MovePos PgGameProcess::PawnMoveDirEnPassat(sf::Vector2i const & Vec, EFigureColor const & eColor)
{
	MovePos kRetPos;
	if (this->bCanEnPassant == false)
	{
		return kRetPos;
	}

	if (!this->CheckEnemyOnPos(this->enPassantPos, eColor))
	{
		return kRetPos;
	}

	if (((int)enPassantPos.y == (int)Vec.y
		&& ((int)Vec.x + 1 == (int)enPassantPos.x)) == false)
	{
		return kRetPos;
	}

	if (eColor == EFC_BLACK)
	{
		CHECK_CAN_MOVE(this->enPassantPos.x, Vec.y - 1, eColor) kRetPos.push_back(sf::Vector2u(this->enPassantPos.x, Vec.y - 1));
	}
	else if (eColor == EFC_WHITE)
	{
		CHECK_CAN_MOVE(this->enPassantPos.x, Vec.y + 1, eColor) kRetPos.push_back(sf::Vector2u(this->enPassantPos.x, Vec.y + 1));
	}
	return kRetPos;
}

MovePos PgGameProcess::PawnMoveDir(sf::Vector2i const& Vec, EFigureColor const& eColor)
{
	MovePos MovePosRet = this->PawnMoveDirEnPassat(Vec, eColor);
	if (eColor == EFC_WHITE)
	{

		if (IsEmptyPos(sf::Vector2u(Vec.x, Vec.y + 1)))
		{
			MovePosRet.push_back(sf::Vector2u(Vec.x, Vec.y + 1));
			if (IsEmptyPos(sf::Vector2u(Vec.x, 3)))
			{
				if (Vec.y == 1) MovePosRet.push_back(sf::Vector2u(Vec.x, 3));
			}
		}

		if (CheckEnemyOnPos(sf::Vector2u(Vec.x + 1, Vec.y + 1), eColor))
		{
			MovePosRet.push_back(sf::Vector2u(Vec.x + 1, Vec.y + 1));
		}

		if (CheckEnemyOnPos(sf::Vector2u(Vec.x - 1, Vec.y + 1), eColor))
		{
			MovePosRet.push_back(sf::Vector2u(Vec.x - 1, Vec.y + 1));
		}
	}
	else
	{
		if (IsEmptyPos(sf::Vector2u(Vec.x, Vec.y - 1)))
		{
			MovePosRet.push_back(sf::Vector2u(Vec.x, Vec.y - 1));
			if (IsEmptyPos(sf::Vector2u(Vec.x, 4)))
			{
				if (Vec.y == 6) MovePosRet.push_back(sf::Vector2u(Vec.x, 4));
			}
		}

		if (CheckEnemyOnPos(sf::Vector2u(Vec.x + 1, Vec.y - 1), eColor))
		{
			MovePosRet.push_back(sf::Vector2u(Vec.x + 1, Vec.y - 1));
		}

		if (CheckEnemyOnPos(sf::Vector2u(Vec.x - 1, Vec.y - 1), eColor))
		{
			MovePosRet.push_back(sf::Vector2u(Vec.x - 1, Vec.y - 1));
		}

	}
	return MovePosRet;
}

MovePos PgGameProcess::HorseMoveDir(sf::Vector2i const& Vec, EFigureColor const& eColor)
{
	MovePos MovePosRet;
	CHECK_CAN_MOVE(Vec.x + 1, Vec.y + 2, eColor)MovePosRet.push_back(sf::Vector2u(Vec.x + 1, Vec.y + 2));
	CHECK_CAN_MOVE(Vec.x - 1, Vec.y + 2, eColor)MovePosRet.push_back(sf::Vector2u(Vec.x - 1, Vec.y + 2));
	CHECK_CAN_MOVE(Vec.x + 1, Vec.y - 2, eColor)MovePosRet.push_back(sf::Vector2u(Vec.x + 1, Vec.y - 2));
	CHECK_CAN_MOVE(Vec.x - 1, Vec.y - 2, eColor)MovePosRet.push_back(sf::Vector2u(Vec.x - 1, Vec.y - 2));
	CHECK_CAN_MOVE(Vec.x + 2, Vec.y + 1, eColor)MovePosRet.push_back(sf::Vector2u(Vec.x + 2, Vec.y + 1));
	CHECK_CAN_MOVE(Vec.x + 2, Vec.y - 1, eColor)MovePosRet.push_back(sf::Vector2u(Vec.x + 2, Vec.y - 1));
	CHECK_CAN_MOVE(Vec.x - 2, Vec.y + 1, eColor)MovePosRet.push_back(sf::Vector2u(Vec.x - 2, Vec.y + 1));
	CHECK_CAN_MOVE(Vec.x - 2, Vec.y - 1, eColor)MovePosRet.push_back(sf::Vector2u(Vec.x - 2, Vec.y - 1));
	return MovePosRet;
}

MovePos PgGameProcess::ElephantMoveDir(sf::Vector2i const& Vec, EFigureColor const& eColor)
{
	MovePos MovePosRet;
	for (int i = Vec.x + 1, j = Vec.y + 1; i < 8; i++, j++)
	{
		IFNBOX_OUT2(i, j) MovePosRet.push_back(sf::Vector2u(i, j));
		if (this->IsEmptyPos(sf::Vector2u(i, j)) == false && Vec != sf::Vector2i(i, j)) { break; }
	}

	for (int i = Vec.x - 1, j = Vec.y - 1; i > -1; i--, j--)
	{
		IFNBOX_OUT2(i, j) MovePosRet.push_back(sf::Vector2u(i, j));
		if (this->IsEmptyPos(sf::Vector2u(i, j)) == false && Vec != sf::Vector2i(i, j)) { break; }
	}

	for (int i = Vec.x + 1, j = Vec.y - 1; i < 8 && j > -1; i++, j--)
	{
		IFNBOX_OUT2(i, j) MovePosRet.push_back(sf::Vector2u(i, j));
		if (this->IsEmptyPos(sf::Vector2u(i, j)) == false && Vec != sf::Vector2i(i, j)) { break; }
	}

	for (int i = Vec.x - 1, j = Vec.y + 1; i > -1 && j < 8; i--, j++)
	{
		IFNBOX_OUT2(i, j) MovePosRet.push_back(sf::Vector2u(i, j));
		if (this->IsEmptyPos(sf::Vector2u(i, j)) == false && Vec != sf::Vector2i(i, j)) { break; }
	}

	return MovePosRet;
}

MovePos PgGameProcess::RookMoveDir(sf::Vector2i const& Vec, EFigureColor const& eColor)
{
	MovePos MovePosRet;
	for (int i = Vec.y; i < 8; i++)
	{
		IFNBOX_OUT2(Vec.x, i) MovePosRet.push_back(sf::Vector2u(Vec.x, i));
		if (this->IsEmptyPos(sf::Vector2u(Vec.x, i)) == false && Vec != sf::Vector2i(Vec.x, i)) { break; }
	}

	for (int i = Vec.x; i < 8; i++)
	{
		IFNBOX_OUT2(Vec.x, i) MovePosRet.push_back(sf::Vector2u(i, Vec.y));
		if (this->IsEmptyPos(sf::Vector2u(i, Vec.y)) == false && Vec != sf::Vector2i(i, Vec.y)) { break; }
	}

	for (int i = Vec.y; i > -1; i--)
	{
		IFNBOX_OUT2(Vec.x, i) MovePosRet.push_back(sf::Vector2u(Vec.x, i));
		if (this->IsEmptyPos(sf::Vector2u(Vec.x, i)) == false && Vec != sf::Vector2i(Vec.x, i)) { break; }
	}

	for (int i = Vec.x; i > -1; i--)
	{
		IFNBOX_OUT2(Vec.x, i) MovePosRet.push_back(sf::Vector2u(i, Vec.y));
		if (this->IsEmptyPos(sf::Vector2u(i, Vec.y)) == false && Vec != sf::Vector2i(i, Vec.y)) { break; }
	}
	return MovePosRet;
}

MovePos PgGameProcess::KingMoveDir(sf::Vector2i const& Vec, EFigureColor const& eColor)
{
	MovePos MovePosRet;
	CHECK_CAN_MOVE(Vec.x + 1, Vec.y + 1, eColor)MovePosRet.push_back(sf::Vector2u(Vec.x + 1, Vec.y + 1));
	CHECK_CAN_MOVE(Vec.x + 1, Vec.y + 1, eColor)MovePosRet.push_back(sf::Vector2u(Vec.x - 1, Vec.y - 1));
	CHECK_CAN_MOVE(Vec.x + 1, Vec.y - 1, eColor)MovePosRet.push_back(sf::Vector2u(Vec.x + 1, Vec.y - 1));
	CHECK_CAN_MOVE(Vec.x - 1, Vec.y + 1, eColor)MovePosRet.push_back(sf::Vector2u(Vec.x - 1, Vec.y + 1));
	CHECK_CAN_MOVE(Vec.x, Vec.y + 1, eColor)MovePosRet.push_back(sf::Vector2u(Vec.x, Vec.y + 1));
	CHECK_CAN_MOVE(Vec.x, Vec.y - 1, eColor)MovePosRet.push_back(sf::Vector2u(Vec.x, Vec.y - 1));
	CHECK_CAN_MOVE(Vec.x - 1, Vec.y, eColor)MovePosRet.push_back(sf::Vector2u(Vec.x - 1, Vec.y));
	CHECK_CAN_MOVE(Vec.x + 1, Vec.y, eColor)MovePosRet.push_back(sf::Vector2u(Vec.x + 1, Vec.y));
	return MovePosRet;
}

MovePos PgGameProcess::QueenMoveDir(sf::Vector2i const& Vec, EFigureColor const& eColor)
{
	MovePos MovePosRet = RookMoveDir(Vec, eColor);
	auto kTemp = ElephantMoveDir(Vec, eColor);
	MovePosRet.insert(MovePosRet.end(), kTemp.begin(), kTemp.end());
	return MovePosRet;
}

void PgGameProcess::ShowWinDlg(EFigureColor const eWhoWinColor)
{
	auto kObjWinBg = g_kObjectMgr.FindObjByID("OBJ_WIN_BG");
	if (kObjWinBg)
	{
		std::string WinTeamName = eWhoWinColor == EFC_WHITE ? "OBJ_WIN_TEXT_WHITE" : "OBJ_WIN_TEXT_BLACK";
		auto kObjTeamColor = g_kObjectMgr.FindObjByID(WinTeamName);
		if (kObjTeamColor)
		{
			kObjWinBg->IsVisible(true);
			kObjTeamColor->IsVisible(true);
		}
	}
}
