#pragma once
#include <map>
#include "tinyxml2\tinyxml2.h"

class PgSoundManager
{
private:
	std::map<std::string, std::string> SoundMap;
public:
	PgSoundManager();
	~PgSoundManager();

	void LoadXml();

	void ReleaseSound(std::string const& strName);
private:
	void ParseAttr(tinyxml2::XMLAttribute const* kAttr);
};

extern PgSoundManager g_kSoundMgr;