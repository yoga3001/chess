#include "stdafx.h"
#include "PgObject.h"
#include "PgUtil.h"
#include "PgConfig.h"

PgObject::PgObject()
{
	//this->kSprite.setColor(sf::Color::Green);
	bIsSelect = false;
	this->m_kID = GenerateID();
}

PgObject::PgObject(std::string strObjID)
{
	bIsSelect = false;
	this->m_kID = strObjID;
}


PgObject::~PgObject()
{
}

void PgObject::SetTextute(std::string const strTexturePath)
{
	kTexture.loadFromFile(strTexturePath.c_str());	
	kTexture.setSmooth(bSmooth);
	kSprite.setTexture(kTexture);
	kSprite.setScale(fScale, fScale);
}

void PgObject::Position(sf::Vector2f const kPos)
{
	this->kSprite.setPosition(kPos);
}

sf::Vector2f PgObject::Position() const
{
	return this->kSprite.getPosition();
}

void PgObject::Select()
{
	this->bIsSelect = true;
	this->kSprite.setColor(sf::Color::Green);
}

void PgObject::DeSelect()
{
	this->bIsSelect = false;
	this->kSprite.setColor(sf::Color::White);
}
