#include "stdafx.h"
#include "PgUtil.h"
#include <ctime>
#include <random>

std::string const GenerateID()
{
	std::string strRet;
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> dist(111, 999);

	for (size_t i = 0; i < 3; i++)
	{
		strRet += std::to_string(dist(gen)) + "-";
	}
	strRet.erase(strRet.end()-1, strRet.end());
	return strRet;
}

std::vector<std::string> SplitString(std::string str, std::regex re)
{
	std::vector<std::string> listRet;
	auto beg_re = std::sregex_iterator(str.begin(), str.end(), re);
	while (beg_re != std::sregex_iterator()) // std::sregex_iterator() is end iter
	{
		listRet.push_back(beg_re->str());
		beg_re++;
	}
	return listRet;
}
