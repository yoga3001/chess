#include "stdafx.h"
#include "PgSoundManager.h"
#include "SFML\Audio.hpp"

PgSoundManager g_kSoundMgr;

PgSoundManager::PgSoundManager()
{
}


PgSoundManager::~PgSoundManager()
{
}

void PgSoundManager::LoadXml()
{
	tinyxml2::XMLDocument kDoc;
	if (kDoc.LoadFile("xml/Sound.Xml") != tinyxml2::XMLError::XML_SUCCESS)
	{
		return;
	}

	auto kElement = kDoc.FirstChildElement();
	while (kElement)
	{
		if (!::strcmp("AUDIO", kElement->Value()))
		{
			auto kChiledElement = kElement->FirstChildElement();
			while (kChiledElement)
			{
				if (!::strcmp("SOUND", kChiledElement->Value()))
				{
					auto kAttr = kChiledElement->FirstAttribute();
					ParseAttr(kAttr);
				}
				kChiledElement = kChiledElement->NextSiblingElement();
			}
		}
		kElement = kElement->NextSiblingElement();
	}
}

void PgSoundManager::ReleaseSound(std::string const & strName)
{
	auto kFind = this->SoundMap.find(strName);
	if (kFind == this->SoundMap.end())
	{
		return;
	}
	//sf::Music kMusic;
	//kMusic.openFromFile(kFind->second);
	//kMusic.play();
	sf::SoundBuffer kSoundBuff;
	kSoundBuff.loadFromFile(kFind->second);
	sf::Sound kSound;
	kSound.setBuffer(kSoundBuff);
	kSound.play();
}

void PgSoundManager::ParseAttr(tinyxml2::XMLAttribute const * kAttr)
{
	if (!kAttr)
	{
		return;
	}

	std::string strID = "";
	std::string strPath = "";
	while (kAttr)
	{
		auto kName = kAttr->Name();
		auto kValued = kAttr->Value();
		if (!::strcmp("ID", kName))
		{
			strID = kValued;
		}
		else if (!::strcmp("PATH", kName))
		{
			strPath = kValued;
		}

		kAttr = kAttr->Next();
	}
	this->SoundMap.insert(std::make_pair(strID, strPath));
}
