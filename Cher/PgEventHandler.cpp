#include "stdafx.h"
#include "PgEventHandler.h"
#include "PgObjectManager.h"
#include "PgRender.h"

PgEventHandler g_kEventHandle;

PgEventHandler::PgEventHandler()
{
	this->bInProcess = true;
}


PgEventHandler::~PgEventHandler()
{
}

void PgEventHandler::Release()
{
	for (auto it : this->kEventStack)
	{
		switch (it.type)
		{
			case sf::Event::Closed:
			{
				this->bInProcess = false;
			}break;
			case sf::Event::MouseButtonPressed:
			{
				if (it.key.code == sf::Mouse::Left)
				{
					g_kObjectMgr.CollideAllObjectWhitPoint(g_kRender.PixetToCoords(g_kRender.GetMousePos()));
				}
			}break;
		}
	}
	this->kEventStack.clear();
}

void PgEventHandler::InsertEvent(sf::Event const kEvent)
{
	this->kEventStack.push_back(kEvent);
}
