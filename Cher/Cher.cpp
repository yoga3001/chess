// Cher.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include "PgRender.h"
#include "PgEventHandler.h"
#include "PgFigure.h"
#include "PgField.h"
#include "PgObjectManager.h"
#include <iostream>
#include "PgGameProcess.h"
#include "PgSoundManager.h"
#include "PgConfig.h"

int main()
{
	LoadConfigXml();
#ifndef _MDd_
	ShowWindow(GetConsoleWindow(), SW_HIDE); // Hide console
#endif // !_MDd_
	g_kRender.Init(sf::VideoMode((unsigned int)(782 * fScale), (unsigned int)(885 * fScale)));
	g_kObjectMgr.LoadXml();
	g_kSoundMgr.LoadXml();
	while (g_kEventHandle.Run())
	{
		g_kGameProcess.HelpUI();
		g_kObjectMgr.one_tick();
		g_kRender.DoRender();
	}
    return 0;
}

