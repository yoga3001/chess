#pragma once
#include "SFML\Graphics.hpp"
#include "PgObjectManager.h"

typedef std::vector<sf::Vector2u> MovePos;

typedef std::vector<PgFigure*> figure_vec_node;

typedef std::vector<figure_vec_node> figure_vec;

class PgGameProcess
{
protected:
	EFigureColor eWhoCourseNow;

	figure_vec kFiledEmulate;

	sf::Vector2u enPassantPos; // special move dir for pawn

	figure_vec_node kDeathFigureStack;

	bool bCanEnPassant;
public:
	PgGameProcess();
	
	~PgGameProcess();

	static sf::Vector2u const ConvPosToCells(sf::Vector2f const inpVec);

	static sf::Vector2f const ConvCellsToPos(sf::Vector2u const inpVec);

	EFigureColor const WhoCourseNow() { return eWhoCourseNow; }

	MovePos const GetAllMovePos(PgFigure* kFigure);
	
	bool CheckCanMoveToPos(PgFigure* kFigure, sf::Vector2u const& kVec);

	void SwapStage();

	void AddFigureToFiled(PgFigure* kFigure);

	bool MoveFigure(PgFigure* kFigure, sf::Vector2f const& kNewPos);

	bool IsEmptyPos(sf::Vector2u const& kVec);

	bool CheckEnemyOnPos(sf::Vector2u const& kVec, EFigureColor const& eColor);

	CLASS_DECLARATION_NO_SET(bool, bIsOver, IsOver)

	void HelpUI();
protected:
	MovePos PawnMoveDirEnPassat(sf::Vector2i const& Vec, EFigureColor const& eColor = EFC_WHITE);

	MovePos PawnMoveDir(sf::Vector2i const& Vec, EFigureColor const& eColor = EFC_WHITE);

	MovePos HorseMoveDir(sf::Vector2i const& Vec, EFigureColor const& eColor = EFC_WHITE);

	MovePos ElephantMoveDir(sf::Vector2i const& Vec, EFigureColor const& eColor = EFC_WHITE);

	MovePos RookMoveDir(sf::Vector2i const& Vec, EFigureColor const& eColor = EFC_WHITE);

	MovePos KingMoveDir(sf::Vector2i const& Vec, EFigureColor const& eColor = EFC_WHITE);

	MovePos QueenMoveDir(sf::Vector2i const& Vec, EFigureColor const& eColor = EFC_WHITE);

	void ShowWinDlg(EFigureColor const eWhoWinColor);
};

extern PgGameProcess g_kGameProcess;