#pragma once
#include "SFML\Graphics\RenderWindow.hpp"
#include <list>
#include "PgObject.h"

class PgRender
{
private:
	sf::RenderWindow m_kWnd;

	std::list<PgObject*> kRenderObject;
public:
	PgRender();

	~PgRender();

	bool Init(sf::VideoMode const &kWindowMode, const char* strWndTitle = "");

	void DoRender();

	void AddDrawObject(PgObject * ObjectPtr, bool bFront = false);
	
	sf::Vector2f const PixetToCoords(sf::Vector2i const vecInp) { return this->m_kWnd.mapPixelToCoords(vecInp); };

	sf::Vector2i const GetMousePos() { return sf::Mouse::getPosition(this->m_kWnd); }
private:
	void EventWork();
};


extern PgRender g_kRender;