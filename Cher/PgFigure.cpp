#include "stdafx.h"
#include "PgFigure.h"
#include "PgGameProcess.h"

PgFigure::PgFigure()
{
}

PgFigure::PgFigure(std::string strObjID) 
	: PgObject(strObjID)
{
}


PgFigure::~PgFigure()
{
}

void PgFigure::SetCustumType(structObjXmlElement const  rhs)
{
	kFigureType = rhs.kFigureType;
	kFigureColor = rhs.kFigureColor;
}

bool PgFigure::Move(sf::Vector2u const vecNewPos)
{
	if (!NBOX_OUT2(vecNewPos.x, vecNewPos.y)) return false;
	this->Position(PgGameProcess::ConvCellsToPos(vecNewPos));
	return true;
}
