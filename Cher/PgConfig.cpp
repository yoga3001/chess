#include "stdafx.h"
#include "PgConfig.h"
#include "tinyxml2\tinyxml2.h"
#include <string>

bool bSmooth = false;
float fScale = 1.f;

void LoadConfigXml()
{
	tinyxml2::XMLDocument kDoc;
	if (kDoc.LoadFile("xml/Config.Xml") != tinyxml2::XMLError::XML_SUCCESS)
	{
		return;
	}
	
	auto kElement = kDoc.FirstChildElement();
	while (kElement)
	{
		if (!::strcmp("CONFIG", kElement->Name()))
		{
			auto kChiledElement = kElement->FirstChildElement();
			while (kChiledElement)
			{
				if (!::strcmp("NODE", kChiledElement->Name()))
				{
					auto kAttr = kChiledElement->FirstAttribute();
					std::string strNodeName = "";
					std::string strNodeValue = "";
					while (kAttr)
					{
						auto kName = kAttr->Name();
						auto kValued = kAttr->Value();
						if (!::strcmp("NAME", kName))
						{
							strNodeName = kValued;
						}
						else if(!::strcmp("VALUE", kName))
						{
							strNodeValue = kValued;
						}
						kAttr = kAttr->Next();
					}

					if (strNodeName == "SCALE")
					{
						fScale = (float)atof(strNodeValue.c_str());
					}
					else if (strNodeName == "SMOOTH")
					{
						bSmooth = (strNodeValue == "TRUE");
					}
				}
				kChiledElement = kChiledElement->NextSiblingElement();
			}
		}
		kElement = kElement->NextSiblingElement();
	}
}
