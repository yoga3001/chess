#pragma once
#include "SFML\Window.hpp"
#include <list>

class PgEventHandler
{
private:
	bool bInProcess;
	std::list<sf::Event> kEventStack;
public:
	PgEventHandler();

	~PgEventHandler();

	void Release();

	void InsertEvent(sf::Event const kEvent);

	bool Run() const { return bInProcess; }
};

extern PgEventHandler g_kEventHandle;
