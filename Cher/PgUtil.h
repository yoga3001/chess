#pragma once
#include <string>
#include <list>
#include <regex>

std::string const GenerateID();

std::vector<std::string> SplitString(std::string str, std::regex re);

template<typename T>
std::vector<T> SplitString(std::string str, std::regex re)
{
	std::vector<T> listRet;
	auto listSplit = SplitString(str, re);
	for (auto it = listSplit.begin(); it != listSplit.end(); it++)
	{
		listRet.push_back((T)atof((*it).c_str()));
	}
	return listRet;
}