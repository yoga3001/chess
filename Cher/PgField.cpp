#include "stdafx.h"
#include "PgField.h"
#include "PgConfig.h"

PgField::PgField()
{
}

PgField::PgField(std::string strObjID) 
	: PgObject(strObjID)
{
}


PgField::~PgField()
{
}

void PgField::SetCustumType(structObjXmlElement const  rhs)
{
}

void PgField::ClearRect()
{
	this->vecFiledRect.clear();
}

void PgField::SelectRect(sf::Vector2u const & vecPoint, sf::Color kColor)
{
	kColor.a = 120;
	sf::RectangleShape rect(sf::Vector2f(90 * fScale, 90 * fScale));
	rect.setFillColor(kColor);
	rect.setPosition((((float)vecPoint.x * 90) + (AJA_FIGURE_X - 13)) * fScale, (((float)vecPoint.y * 90) + AJA_FIGURE_Y + 6) * fScale);
	this->vecFiledRect.push_back(rect);
}
