﻿========================================================================
    КОНСОЛЬНОЕ ПРИЛОЖЕНИЕ. Обзор проекта CherServer
========================================================================

Это приложение CherServer создано автоматически с помощью мастера приложений.

В этом файле представлена сводка содержимого всех файлов, входящих в состав приложения CherServer.


CherServer.vcxproj
    Это основной файл проекта VC++, создаваемый с помощью мастера приложений. Он содержит данные о версии языка Visual C++, использованной для создания файла, а также сведения о платформах, конфигурациях и функциях проекта, выбранных с помощью мастера приложений.

CherServer.vcxproj.filters
    Это файл фильтров для проектов VC++, созданный с помощью мастера приложений. Он содержит сведения о сопоставлениях между файлами в вашем проекте и фильтрами. Эти сопоставления используются в среде IDE для группировки файлов с одинаковыми расширениями в одном узле (например CPP-файлы сопоставляются с фильтром "Исходные файлы").

CherServer.cpp
    Это основной исходный файл приложения.

/////////////////////////////////////////////////////////////////////////////
Другие стандартные файлы:

StdAfx.h, StdAfx.cpp
    Эти файлы используются для построения файла предкомпилированного заголовка (PCH) с именем CherServer.pch и файла предкомпилированных типов с именем StdAfx.obj.

/////////////////////////////////////////////////////////////////////////////
Прочие примечания.

С помощью комментариев «TODO:» в мастере приложений обозначаются фрагменты исходного кода, которые необходимо дополнить или изменить.

/////////////////////////////////////////////////////////////////////////////
